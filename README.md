# Análise Dados

Sistema que analisa e processa dados informados por arquivos de extensão .dat.
Projeto foi desenvolvido em Maven Project com Java 8.

## Getting Started

Para executar a aplicação, basta rodar a classe br.com.cwi.analisedados.Main. 
A aplicação fica escutando, a cada meio segundo, se arquivos com extensão .dat foram criados em %HOMEPATH%/in.
Quando arquivos novos são criados, a aplicação junta todas as informações e salva em %HOMEPATH%/in/arquivo.dat.temp. 
A cada cinco segundos, a aplicação lê e processa os dados do arquivo %HOMEPATH%/in/arquivo.dat.temp.
Os dados processados são salvos em %HOMEPATH%/out/arquivo.done.dat.

### Prerequisites

Eclipse e Java instalados.

## Running the tests

Para rodar os testes, basta executar o comando "mvn test".
