package br.com.cwi.analisedados;

import static br.com.cwi.analisedados.application.utils.Constantes.EXTENSAO_DAT;
import static br.com.cwi.analisedados.application.utils.Constantes.EXTENSAO_DAT_TEMP;
import static br.com.cwi.analisedados.application.utils.Constantes.PATH_ARQUIVO_DONE;
import static br.com.cwi.analisedados.application.utils.Constantes.PATH_ARQUIVO_TEMP;
import static br.com.cwi.analisedados.application.utils.Constantes.PATH_DATA_IN;
import static br.com.cwi.analisedados.application.utils.Constantes.USER_HOME;

import org.apache.commons.io.monitor.FileAlterationListener;

import br.com.cwi.analisedados.application.service.arquivo.geracao.ArquivoServiceGeracaoListener;
import br.com.cwi.analisedados.application.service.arquivo.geracao.relatorio.ArquivoRelatorioServiceGeracaoImpl;
import br.com.cwi.analisedados.application.service.arquivo.geracao.temporario.ArquivoTemporarioServiceGeracaoImpl;
import br.com.cwi.analisedados.application.service.arquivo.observador.ArquivoServiceObservador;
import br.com.cwi.analisedados.application.service.arquivo.observador.ArquivoServiceObservadorImpl;

public class Main {

  public static void main(String[] args) throws Exception {
    ArquivoServiceObservador arquivoServiceLeitura = new ArquivoServiceObservadorImpl();
    ArquivoServiceGeracaoListener arquivoTemporarioServiceLeituraEscrita = new ArquivoTemporarioServiceGeracaoImpl();
    ArquivoServiceGeracaoListener arquivoRelatorioServiceGeracao = new ArquivoRelatorioServiceGeracaoImpl();

    String userHome = System.getProperty(USER_HOME);
    String pathLeitura = userHome.concat(PATH_DATA_IN);

    FileAlterationListener arquivoTemporariolistener = arquivoTemporarioServiceLeituraEscrita.gerarListener(EXTENSAO_DAT, userHome.concat(PATH_ARQUIVO_TEMP));
    FileAlterationListener relatorioListener = arquivoRelatorioServiceGeracao.gerarListener(EXTENSAO_DAT_TEMP, userHome.concat(PATH_ARQUIVO_DONE));

    arquivoServiceLeitura.observar(pathLeitura, 500l, arquivoTemporariolistener);
    arquivoServiceLeitura.observar(pathLeitura, 5000l, relatorioListener);
  }

}
