package br.com.cwi.analisedados.application.exception;

public class BussinesException extends RuntimeException {

  private static final long serialVersionUID = -9157196687607879008L;

  public BussinesException() {
    super();
  }

  public BussinesException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

  public BussinesException(String message, Throwable cause) {
    super(message, cause);
  }

  public BussinesException(String message) {
    super(message);
  }

  public BussinesException(Throwable cause) {
    super(cause);
  }

}
