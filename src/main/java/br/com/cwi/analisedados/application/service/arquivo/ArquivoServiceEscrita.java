package br.com.cwi.analisedados.application.service.arquivo;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

public interface ArquivoServiceEscrita extends Serializable {

  void escrever(final String pathArquivoEscrita, final List<String> linhas, final boolean adicionarAoFim) throws IOException;

}
