package br.com.cwi.analisedados.application.service.arquivo;

import static org.apache.commons.io.FileUtils.writeLines;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class ArquivoServiceEscritaImpl implements ArquivoServiceEscrita {

  private static final long serialVersionUID = -500431481653708582L;

  @Override
  public void escrever(final String pathArquivoEscrita, final List<String> linhas, final boolean adicionarAoFim) throws IOException {
    writeLines(new File(pathArquivoEscrita), linhas, adicionarAoFim);
  }

}
