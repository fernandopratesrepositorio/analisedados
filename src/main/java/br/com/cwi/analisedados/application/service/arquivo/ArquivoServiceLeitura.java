package br.com.cwi.analisedados.application.service.arquivo;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

public interface ArquivoServiceLeitura extends Serializable {

  List<String> ler(final File file, final String extensaoLeitura) throws IOException;

}
