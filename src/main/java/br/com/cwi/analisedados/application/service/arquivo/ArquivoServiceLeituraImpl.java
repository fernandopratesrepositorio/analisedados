package br.com.cwi.analisedados.application.service.arquivo;

import static org.apache.commons.io.FileUtils.readLines;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import br.com.cwi.analisedados.application.utils.pathmatcher.PathMatcherHelper;

public class ArquivoServiceLeituraImpl implements ArquivoServiceLeitura {

  private static final long serialVersionUID = -6482202720515158803L;

  @Override
  public List<String> ler(final File file, final String extensaoLeitura) throws IOException {
    if (file == null || extensaoLeitura == null) {
      return new ArrayList<>();
    }
    if (!PathMatcherHelper.validar(extensaoLeitura, file.toPath())) {
      return new ArrayList<>();
    }
    return readLines(file, Charset.defaultCharset());
  }
}
