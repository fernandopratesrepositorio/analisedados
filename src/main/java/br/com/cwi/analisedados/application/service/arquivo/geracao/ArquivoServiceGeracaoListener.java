package br.com.cwi.analisedados.application.service.arquivo.geracao;

import java.io.Serializable;

import org.apache.commons.io.monitor.FileAlterationListener;

public interface ArquivoServiceGeracaoListener extends Serializable {

  FileAlterationListener gerarListener(final String extensaoLeitura, final String pathArquivoEscrita);

}
