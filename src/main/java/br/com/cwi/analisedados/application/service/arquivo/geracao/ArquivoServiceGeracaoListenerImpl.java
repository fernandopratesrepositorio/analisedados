package br.com.cwi.analisedados.application.service.arquivo.geracao;

import java.io.File;

import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationListenerAdaptor;

import br.com.cwi.analisedados.application.service.arquivo.ArquivoServiceEscrita;
import br.com.cwi.analisedados.application.service.arquivo.ArquivoServiceEscritaImpl;
import br.com.cwi.analisedados.application.service.arquivo.ArquivoServiceLeitura;
import br.com.cwi.analisedados.application.service.arquivo.ArquivoServiceLeituraImpl;

public abstract class ArquivoServiceGeracaoListenerImpl implements ArquivoServiceGeracaoListener {

  private static final long serialVersionUID = -7827248968799904075L;

  protected ArquivoServiceEscrita arquivoServiceEscrita = new ArquivoServiceEscritaImpl();

  protected ArquivoServiceLeitura arquivoServiceLeitura = new ArquivoServiceLeituraImpl();

  protected void executionOnFileCreate(File file, String extensaoLeitura, String pathArquivoEscrita) {
  }

  protected void executiononFileChange(File file, String extensaoLeitura, String pathArquivoEscrita) {
  }

  @Override
  public FileAlterationListener gerarListener(final String extensaoLeitura, final String pathArquivoEscrita) {
    return new FileAlterationListenerAdaptor() {
      @Override
      public void onFileCreate(final File file) {
        executionOnFileCreate(file, extensaoLeitura, pathArquivoEscrita);
      }

      @Override
      public void onFileChange(File file) {
        executiononFileChange(file, extensaoLeitura, pathArquivoEscrita);
      }
    };
  }

}
