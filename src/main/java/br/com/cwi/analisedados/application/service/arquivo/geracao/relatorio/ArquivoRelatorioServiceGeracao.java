package br.com.cwi.analisedados.application.service.arquivo.geracao.relatorio;

import java.io.Serializable;

import org.apache.commons.io.monitor.FileAlterationListener;

public interface ArquivoRelatorioServiceGeracao extends Serializable {

  FileAlterationListener gerarListener(final String extensaoLeitura, final String pathArquivoEscrita);

}
