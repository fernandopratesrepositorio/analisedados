package br.com.cwi.analisedados.application.service.arquivo.geracao.relatorio;

import java.io.File;
import java.io.IOException;
import java.util.List;

import br.com.cwi.analisedados.application.exception.BussinesException;
import br.com.cwi.analisedados.application.service.arquivo.geracao.ArquivoServiceGeracaoListenerImpl;
import br.com.cwi.analisedados.application.service.relatorio.RelatorioAnaliseDadosServiceGeracao;
import br.com.cwi.analisedados.application.service.relatorio.RelatorioAnaliseDadosServiceGeracaoImpl;

public class ArquivoRelatorioServiceGeracaoImpl extends ArquivoServiceGeracaoListenerImpl {

  private static final long serialVersionUID = -3683016515850987193L;

  private RelatorioAnaliseDadosServiceGeracao relatorioAnaliseDadosServiceGeracao = new RelatorioAnaliseDadosServiceGeracaoImpl();

  @Override
  protected void executionOnFileCreate(File file, String extensaoLeitura, String pathArquivoEscrita) {
    executionOnFile(file, extensaoLeitura, pathArquivoEscrita);
  }

  @Override
  protected void executiononFileChange(File file, String extensaoLeitura, String pathArquivoEscrita) {
    executionOnFile(file, extensaoLeitura, pathArquivoEscrita);
  }

  private void executionOnFile(File file, String extensaoLeitura, String pathArquivoEscrita) {
    try {
      List<String> linhas = arquivoServiceLeitura.ler(file, extensaoLeitura);
      if (linhas == null || linhas.isEmpty()) {
        return;
      }

      arquivoServiceEscrita.escrever(pathArquivoEscrita, relatorioAnaliseDadosServiceGeracao.gerar(linhas), false);
    } catch (IOException e) {
      throw new BussinesException(e);
    }
  }

}
