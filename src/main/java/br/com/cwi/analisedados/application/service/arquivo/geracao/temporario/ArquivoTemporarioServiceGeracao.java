package br.com.cwi.analisedados.application.service.arquivo.geracao.temporario;

import java.io.Serializable;

import org.apache.commons.io.monitor.FileAlterationListener;

public interface ArquivoTemporarioServiceGeracao extends Serializable {

  FileAlterationListener gerarListener(final String extensaoLeitura, final String pathArquivoEscrita);

}
