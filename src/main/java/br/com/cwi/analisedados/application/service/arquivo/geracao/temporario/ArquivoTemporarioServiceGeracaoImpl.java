package br.com.cwi.analisedados.application.service.arquivo.geracao.temporario;

import java.io.File;
import java.io.IOException;
import java.util.List;

import br.com.cwi.analisedados.application.exception.BussinesException;
import br.com.cwi.analisedados.application.service.arquivo.geracao.ArquivoServiceGeracaoListenerImpl;

public class ArquivoTemporarioServiceGeracaoImpl extends ArquivoServiceGeracaoListenerImpl {

  private static final long serialVersionUID = 5404158518479894185L;

  @Override
  protected void executionOnFileCreate(File file, String extensaoLeitura, final String pathArquivoEscrita) {
    try {
      List<String> linhas = arquivoServiceLeitura.ler(file, extensaoLeitura);
      arquivoServiceEscrita.escrever(pathArquivoEscrita, linhas, true);
    } catch (IOException e) {
      throw new BussinesException(e);
    }
  }

}
