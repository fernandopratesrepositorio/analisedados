package br.com.cwi.analisedados.application.service.arquivo.observador;

import java.io.Serializable;

import org.apache.commons.io.monitor.FileAlterationListener;

public interface ArquivoServiceObservador extends Serializable {

  void observar(final String pathLeitura, final long intervaloLeitura, final FileAlterationListener listener);

}
