package br.com.cwi.analisedados.application.service.arquivo.observador;

import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;

import br.com.cwi.analisedados.application.exception.BussinesException;

public class ArquivoServiceObservadorImpl implements ArquivoServiceObservador {

  private static final long serialVersionUID = 8666140664811115615L;

  @Override
  public void observar(final String pathLeitura, final long intervaloLeitura, final FileAlterationListener listener) {
    FileAlterationObserver observer = new FileAlterationObserver(pathLeitura);
    observer.addListener(listener);

    FileAlterationMonitor monitor = new FileAlterationMonitor(intervaloLeitura);
    monitor.addObserver(observer);
    try {
      monitor.start();
    } catch (Exception e) {
      throw new BussinesException(e);
    }
  }

}
