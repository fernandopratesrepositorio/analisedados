package br.com.cwi.analisedados.application.service.relatorio;

import java.io.Serializable;
import java.util.List;

public interface RelatorioAnaliseDadosServiceGeracao extends Serializable {

  List<String> gerar(final List<String> dados);

}
