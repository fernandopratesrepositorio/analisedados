package br.com.cwi.analisedados.application.service.relatorio;

import static br.com.cwi.analisedados.application.utils.Mensagens.ID_VENDA_MAIS_CARA;
import static br.com.cwi.analisedados.application.utils.Mensagens.O_PIOR_VENDEDOR_JA;
import static br.com.cwi.analisedados.application.utils.Mensagens.QUANTIDADE_CLIENTES_ARQUIVO_ENTRADA;
import static br.com.cwi.analisedados.application.utils.Mensagens.QUANTIDADE_VENDEDOR_ARQUIVO_ENTRADA;
import static java.util.Comparator.comparing;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import br.com.cwi.analisedados.application.utils.dadosclientes.DadosClientesHelper;
import br.com.cwi.analisedados.application.utils.dadosvendas.DadosVendasHelper;
import br.com.cwi.analisedados.application.utils.dadosvendedores.DadosVendedoresHelper;
import br.com.cwi.analisedados.domain.model.cliente.Cliente;
import br.com.cwi.analisedados.domain.model.dadosvenda.DadosVenda;
import br.com.cwi.analisedados.domain.model.vendedor.Vendedor;

public class RelatorioAnaliseDadosServiceGeracaoImpl implements RelatorioAnaliseDadosServiceGeracao {

  private static final long serialVersionUID = -3590187450851179885L;

  @Override
  public List<String> gerar(List<String> dados) {
    if (dados == null || dados.isEmpty()) {
      return new ArrayList<>();
    }

    List<Cliente> cliente = DadosClientesHelper.obter(dados);
    List<Vendedor> vendedores = DadosVendedoresHelper.obter(dados);
    List<DadosVenda> vendas = DadosVendasHelper.obter(dados);

    List<String> relatorioDados = new LinkedList<>();

    String quantidadeClientesArquivoEntrada = String.valueOf(cliente.size());
    relatorioDados.add(QUANTIDADE_CLIENTES_ARQUIVO_ENTRADA.concat(quantidadeClientesArquivoEntrada));

    String quantidadeVendedoresArquivoEntrada = String.valueOf(vendedores.size());
    relatorioDados.add(QUANTIDADE_VENDEDOR_ARQUIVO_ENTRADA.concat(quantidadeVendedoresArquivoEntrada));

    DadosVenda vendaMaisCara = vendas.stream().sorted(comparing(DadosVenda::getValorTotal).reversed()).findFirst().orElse(null);
    String idVendaMaisCara = vendaMaisCara != null ? vendaMaisCara.getId().toString() : null;
    relatorioDados.add(ID_VENDA_MAIS_CARA.concat(idVendaMaisCara));

    DadosVenda piorVendedor = vendas.stream().sorted(comparing(DadosVenda::getValorTotal)).findFirst().orElse(null);
    String nomePiorVendedor = piorVendedor != null ? piorVendedor.getNomeVendedor() : null;
    relatorioDados.add(O_PIOR_VENDEDOR_JA.concat(nomePiorVendedor));

    return relatorioDados;
  }

}
