package br.com.cwi.analisedados.application.utils;

public interface Constantes {

  String USER_HOME = "user.home";

  String PATH_DATA_IN = "/data/in/";

  String PATH_DATA_OUT = "/data/out/";

  String PATH_ARQUIVO_TEMP = PATH_DATA_IN.concat("arquivo.dat.temp");

  String PATH_ARQUIVO_DONE = PATH_DATA_OUT.concat("arquivo.done.dat");

  String EXTENSAO_DAT = ".dat";
  
  String EXTENSAO_DAT_TEMP = ".dat.temp";

  String GLOB_PATTERN = "glob:**";

  String CEDILHA = "ç";

  String COLCHETE_ABERTO = "[";

  String COLCHETE_FECHADO = "]";

  String HIFEN = "-";

  String VIRGULA = ",";

}
