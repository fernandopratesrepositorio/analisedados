package br.com.cwi.analisedados.application.utils;

public interface Mensagens {

  String QUANTIDADE_CLIENTES_ARQUIVO_ENTRADA = "Quantidade de clientes no arquivo de entrada: ";

  String QUANTIDADE_VENDEDOR_ARQUIVO_ENTRADA = "Quantidade de vendedor no arquivo de entrada: ";

  String ID_VENDA_MAIS_CARA = "ID da venda mais cara: ";

  String O_PIOR_VENDEDOR_JA = "O pior vendedor já: ";

}
