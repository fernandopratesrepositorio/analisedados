package br.com.cwi.analisedados.application.utils.dadosclientes;

import static br.com.cwi.analisedados.application.utils.Constantes.CEDILHA;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import br.com.cwi.analisedados.domain.model.EnumDados;
import br.com.cwi.analisedados.domain.model.cliente.Cliente;

public class DadosClientesHelper {

  private static final int TAMANHO_DADO_CORRETO = 4;

  private static final int POSICAO_CODIGO = 0;

  private static final int POSICAO_CNPJ = 1;

  private static final int POSICAO_NOME = 2;

  private static final int POSICAO_AREA_TRABALHO = 3;

  private DadosClientesHelper() {
  }

  public static List<Cliente> obter(List<String> dados) {
    if (dados == null || dados.isEmpty()) {
      return new ArrayList<>();
    }

    List<Cliente> clientes = new ArrayList<>();

    dados.stream().filter(Objects::nonNull).forEach(dado -> {
      Cliente cliente = getCliente(dado);
      if (cliente != null) {
        clientes.add(cliente);
      }
    });

    clientes.sort(Comparator.comparing(Cliente::getCnpj));

    return clientes;
  }

  private static Cliente getCliente(String dado) {
    String[] dadosSplit = dado.split(CEDILHA);

    if (dadosSplit.length != TAMANHO_DADO_CORRETO) {
      return null;
    }

    String codigo = dadosSplit[POSICAO_CODIGO];
    if (!EnumDados.CLIENTE.getCodigo().equals(codigo)) {
      return null;
    }

    return new Cliente(Long.valueOf(dadosSplit[POSICAO_CNPJ]), dadosSplit[POSICAO_NOME], dadosSplit[POSICAO_AREA_TRABALHO]);
  }

}
