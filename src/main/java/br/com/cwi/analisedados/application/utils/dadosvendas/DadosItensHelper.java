package br.com.cwi.analisedados.application.utils.dadosvendas;

import static br.com.cwi.analisedados.application.utils.Constantes.COLCHETE_ABERTO;
import static br.com.cwi.analisedados.application.utils.Constantes.COLCHETE_FECHADO;
import static br.com.cwi.analisedados.application.utils.Constantes.HIFEN;
import static br.com.cwi.analisedados.application.utils.Constantes.VIRGULA;
import static java.util.Arrays.asList;
import static org.apache.commons.lang3.StringUtils.EMPTY;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import br.com.cwi.analisedados.domain.model.dadosvenda.Item;

public class DadosItensHelper {

  private static final int TAMANHO_DADO_CORRETO = 3;

  private static final int POSICAO_ID = 0;

  private static final int POSICAO_QUANTIDADE = 1;

  private static final int POSICAO_VALOR_UNITARIO = 2;

  private DadosItensHelper() {
  }

  public static List<Item> obter(String dados) {
    if (dados == null || dados.isEmpty()) {
      return new ArrayList<>();
    }

    List<Item> itens = new ArrayList<>();

    dados = removerCaracteresNaoUtilizados(dados);

    String[] dadosSplit = dados.split(VIRGULA);

    asList(dadosSplit).stream().filter(Objects::nonNull).forEach(dado -> {
      Item item = getItem(dado);
      if (item != null) {
        itens.add(item);
      }
    });

    itens.sort(Comparator.comparing(Item::getId));

    return itens;
  }

  private static Item getItem(String dado) {
    String[] dadosSplit = dado.split(HIFEN);

    if (dadosSplit.length != TAMANHO_DADO_CORRETO) {
      return null;
    }

    return new Item(Long.valueOf(dadosSplit[POSICAO_ID]), Long.valueOf(dadosSplit[POSICAO_QUANTIDADE]), Double.valueOf(dadosSplit[POSICAO_VALOR_UNITARIO]));
  }

  private static String removerCaracteresNaoUtilizados(String dados) {
    return dados.replace(COLCHETE_ABERTO, EMPTY).replace(COLCHETE_FECHADO, EMPTY);
  }

}
