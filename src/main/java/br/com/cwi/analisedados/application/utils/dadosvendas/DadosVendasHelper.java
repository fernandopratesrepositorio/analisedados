package br.com.cwi.analisedados.application.utils.dadosvendas;

import static br.com.cwi.analisedados.application.utils.Constantes.CEDILHA;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import br.com.cwi.analisedados.domain.model.EnumDados;
import br.com.cwi.analisedados.domain.model.dadosvenda.DadosVenda;
import br.com.cwi.analisedados.domain.model.dadosvenda.Item;

public class DadosVendasHelper {

  private static final int TAMANHO_DADO_CORRETO = 4;

  private static final int POSICAO_CODIGO = 0;

  private static final int POSICAO_ID = 1;

  private static final int POSICAO_ITENS = 2;

  private static final int POSICAO_NOME_VENDEDOR = 3;

  private DadosVendasHelper() {
  }

  public static List<DadosVenda> obter(List<String> dados) {
    if (dados == null || dados.isEmpty()) {
      return new ArrayList<>();
    }

    List<DadosVenda> vendas = new ArrayList<>();

    dados.stream().filter(Objects::nonNull).forEach(dado -> {
      DadosVenda dadosVenda = getDadosVenda(dado);
      if (dadosVenda != null) {
        vendas.add(dadosVenda);
      }
    });

    vendas.sort(Comparator.comparing(DadosVenda::getId));

    return vendas;
  }

  private static DadosVenda getDadosVenda(String dado) {
    String[] dadosSplit = dado.split(CEDILHA);

    if (dadosSplit.length != TAMANHO_DADO_CORRETO) {
      return null;
    }

    String codigo = dadosSplit[POSICAO_CODIGO];
    if (!EnumDados.DADOS_VENDA.getCodigo().equals(codigo)) {
      return null;
    }

    List<Item> itens = DadosItensHelper.obter(dadosSplit[POSICAO_ITENS]);

    return new DadosVenda(Long.valueOf(dadosSplit[POSICAO_ID]), dadosSplit[POSICAO_NOME_VENDEDOR], itens);
  }

}
