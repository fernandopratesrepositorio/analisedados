package br.com.cwi.analisedados.application.utils.dadosvendedores;

import static br.com.cwi.analisedados.application.utils.Constantes.CEDILHA;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import br.com.cwi.analisedados.domain.model.EnumDados;
import br.com.cwi.analisedados.domain.model.vendedor.Vendedor;

public class DadosVendedoresHelper {

  private static final int TAMANHO_DADO_CORRETO = 4;

  private static final int POSICAO_CODIGO = 0;

  private static final int POSICAO_CPF = 1;

  private static final int POSICAO_NOME = 2;

  private static final int POSICAO_SALARIO = 3;

  private DadosVendedoresHelper() {
  }

  public static List<Vendedor> obter(List<String> dados) {
    if (dados == null || dados.isEmpty()) {
      return new ArrayList<>();
    }

    List<Vendedor> vendedores = new ArrayList<>();

    dados.stream().filter(Objects::nonNull).forEach(dado -> {
      Vendedor vendedor = getVendedor(dado);
      if (vendedor != null) {
        vendedores.add(vendedor);
      }
    });

    vendedores.sort(Comparator.comparing(Vendedor::getCpf));

    return vendedores;
  }

  private static Vendedor getVendedor(String dado) {
    String[] dadosSplit = dado.split(CEDILHA);

    if (dadosSplit.length != TAMANHO_DADO_CORRETO) {
      return null;
    }

    String codigo = dadosSplit[POSICAO_CODIGO];
    if (!EnumDados.VENDEDOR.getCodigo().equals(codigo)) {
      return null;
    }

    return new Vendedor(Long.valueOf(dadosSplit[POSICAO_CPF]), dadosSplit[POSICAO_NOME], Double.valueOf(dadosSplit[POSICAO_SALARIO]));
  }

}
