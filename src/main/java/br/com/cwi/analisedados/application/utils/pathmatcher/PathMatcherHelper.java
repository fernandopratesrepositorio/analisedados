package br.com.cwi.analisedados.application.utils.pathmatcher;

import static br.com.cwi.analisedados.application.utils.Constantes.GLOB_PATTERN;
import static java.nio.file.FileSystems.getDefault;

import java.nio.file.Path;

import org.apache.commons.lang3.StringUtils;

public class PathMatcherHelper {

  private PathMatcherHelper() {
  }

  public static boolean validar(final String estensaoArquivo, final Path path) {
    if (StringUtils.isEmpty(estensaoArquivo) || path == null) {
      return false;
    }
    return getDefault().getPathMatcher(GLOB_PATTERN.concat(estensaoArquivo)).matches(path);
  }

}
