package br.com.cwi.analisedados.domain.model;

public enum EnumDados
{

 VENDEDOR ("001"),
 CLIENTE ("002"),
 DADOS_VENDA ("003");

  private String codigo;

  private EnumDados(String codigo) {
    this.codigo = codigo;
  }

  public String getCodigo() {
    return codigo;
  }

}
