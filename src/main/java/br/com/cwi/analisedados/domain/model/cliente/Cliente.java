package br.com.cwi.analisedados.domain.model.cliente;

import java.io.Serializable;

public class Cliente implements Serializable {

  private static final long serialVersionUID = 7240377925653061929L;

  private Long cnpj;

  private String nome;

  private String areaTrabalho;

  public Cliente(Long cnpj, String nome, String areaTrabalho) {
    super();
    this.cnpj = cnpj;
    this.nome = nome;
    this.areaTrabalho = areaTrabalho;
  }

  public Long getCnpj() {
    return cnpj;
  }

  public String getNome() {
    return nome;
  }

  public String getAreaTrabalho() {
    return areaTrabalho;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((areaTrabalho == null) ? 0 : areaTrabalho.hashCode());
    result = prime * result + ((cnpj == null) ? 0 : cnpj.hashCode());
    result = prime * result + ((nome == null) ? 0 : nome.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    return this.hashCode() == obj.hashCode();
  }

}
