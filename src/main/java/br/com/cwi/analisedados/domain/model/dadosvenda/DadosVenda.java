package br.com.cwi.analisedados.domain.model.dadosvenda;

import java.io.Serializable;
import java.util.List;

public class DadosVenda implements Serializable {

  private static final long serialVersionUID = -3956302734833599739L;

  private Long id;

  private String nomeVendedor;

  private List<Item> itens;

  private Double valorTotal;

  public DadosVenda(Long id, String nomeVendedor, List<Item> itens) {
    super();
    this.id = id;
    this.nomeVendedor = nomeVendedor;
    this.itens = itens;
    calcularValorTotal();
  }

  private void calcularValorTotal() {
    if (itens == null || itens.isEmpty()) {
      return;
    }
    valorTotal = itens.stream().mapToDouble(Item::getValorTotal).sum();
  }

  public Long getId() {
    return id;
  }

  public String getNomeVendedor() {
    return nomeVendedor;
  }

  public List<Item> getItens() {
    return itens;
  }

  public Double getValorTotal() {
    return valorTotal;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((itens == null) ? 0 : itens.hashCode());
    result = prime * result + ((nomeVendedor == null) ? 0 : nomeVendedor.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    return this.hashCode() == obj.hashCode();
  }

}
