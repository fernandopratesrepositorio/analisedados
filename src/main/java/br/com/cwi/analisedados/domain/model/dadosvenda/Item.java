package br.com.cwi.analisedados.domain.model.dadosvenda;

import java.io.Serializable;

public class Item implements Serializable {

  private static final long serialVersionUID = 2926417948601889569L;

  private Long id;

  private Long quantidade;

  private Double valorUnitario;

  private Double valorTotal;

  public Item(Long id, Long quantidade, Double valorUnitario) {
    super();
    this.id = id;
    this.quantidade = quantidade;
    this.valorUnitario = valorUnitario;
    calcularValorTotal();
  }

  private void calcularValorTotal() {
    if (quantidade == null || valorUnitario == null) {
      return;
    }
    valorTotal = quantidade * valorUnitario;
  }

  public Long getId() {
    return id;
  }

  public Long getQuantidade() {
    return quantidade;
  }

  public Double getValorUnitario() {
    return valorUnitario;
  }

  public Double getValorTotal() {
    return valorTotal;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((valorUnitario == null) ? 0 : valorUnitario.hashCode());
    result = prime * result + ((quantidade == null) ? 0 : quantidade.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    return this.hashCode() == obj.hashCode();
  }

}
