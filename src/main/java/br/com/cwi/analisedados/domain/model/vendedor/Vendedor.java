package br.com.cwi.analisedados.domain.model.vendedor;

import java.io.Serializable;

public class Vendedor implements Serializable {

  private static final long serialVersionUID = -7107928343716992948L;

  private Long cpf;

  private String nome;

  private Double salario;

  public Vendedor(Long cpf, String nome, Double salario) {
    super();
    this.cpf = cpf;
    this.nome = nome;
    this.salario = salario;
  }

  public Long getCpf() {
    return cpf;
  }

  public String getNome() {
    return nome;
  }

  public Double getSalario() {
    return salario;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((cpf == null) ? 0 : cpf.hashCode());
    result = prime * result + ((nome == null) ? 0 : nome.hashCode());
    result = prime * result + ((salario == null) ? 0 : salario.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    return this.hashCode() == obj.hashCode();
  }

}
