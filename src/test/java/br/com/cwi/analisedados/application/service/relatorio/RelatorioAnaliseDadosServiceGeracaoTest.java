package br.com.cwi.analisedados.application.service.relatorio;

import static br.com.cwi.analisedados.application.utils.Mensagens.ID_VENDA_MAIS_CARA;
import static br.com.cwi.analisedados.application.utils.Mensagens.O_PIOR_VENDEDOR_JA;
import static br.com.cwi.analisedados.application.utils.Mensagens.QUANTIDADE_CLIENTES_ARQUIVO_ENTRADA;
import static br.com.cwi.analisedados.application.utils.Mensagens.QUANTIDADE_VENDEDOR_ARQUIVO_ENTRADA;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class RelatorioAnaliseDadosServiceGeracaoTest {

  @Test
  public void deveProcessarDados() {
    //given
    List<String> dados = asList("001ç1234567891234çDiegoç50000",
                                "001ç3245678865434çRenatoç40000.99",
                                "001ç3245678865431çRenato1ç10000.99",
                                "001ç3245678865432çRenato2ç20000.99",
                                "001ç3245678865433çRenato3ç30000.99",
                                "002ç2345675434544345çJose da SilvaçRural",
                                "002ç2345675433444345çEduardo PereiraçRural",
                                "002ç2345675433444341çTomasçTI",
                                "003ç10ç[1-10-100,2-30-2.50,3-40-3.10]çDiego",
                                "003ç08ç[1-34-10,2-33-1.50,3-40-0.10]çRenato",
                                null,
                                "",
                                "123213123eda");
    RelatorioAnaliseDadosServiceGeracao relatorioAnaliseDadosServiceGeracao = new RelatorioAnaliseDadosServiceGeracaoImpl();

    //when
    List<String> relatorioDados = relatorioAnaliseDadosServiceGeracao.gerar(dados);

    //then
    Assert.assertEquals(4, relatorioDados.size());

    String quantidadeClientesArquivoEntrada = relatorioDados.get(0);
    assertEquals(QUANTIDADE_CLIENTES_ARQUIVO_ENTRADA.concat("3"), quantidadeClientesArquivoEntrada);

    String quantidadeVendedoresArquivoEntrada = relatorioDados.get(1);
    assertEquals(QUANTIDADE_VENDEDOR_ARQUIVO_ENTRADA.concat("5"), quantidadeVendedoresArquivoEntrada);

    String idVendaMaisCara = relatorioDados.get(2);
    assertEquals(ID_VENDA_MAIS_CARA.concat("10"), idVendaMaisCara);

    String piorVendedor = relatorioDados.get(3);
    assertEquals(O_PIOR_VENDEDOR_JA.concat("Renato"), piorVendedor);
  }

}
