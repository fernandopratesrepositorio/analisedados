package br.com.cwi.analisedados.application.utils.dadosclientes;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import br.com.cwi.analisedados.domain.model.cliente.Cliente;

public class DadosClientesHelperTest {

  @Test
  public void deveValidarDadosQuandoNull() {
    //given
    List<String> dados = null;

    //when
    List<Cliente> clientes = DadosClientesHelper.obter(dados);

    //then
    assertTrue(clientes.isEmpty());
  }

  @Test
  public void deveValidarDadosQuandoEmpty() {
    //given
    List<String> dados = new ArrayList<>();

    //when
    List<Cliente> clientes = DadosClientesHelper.obter(dados);

    //then
    assertTrue(clientes.isEmpty());
  }

  @Test
  public void deveObterDadosVendedores() {
    //given
    List<String> dados = asList("002ç2345675434544345çJose da SilvaçRural",
                                "001ç3245678865434çRenatoç40000.99",
                                "002ç2345675433444345çEduardo PereiraçRural",
                                "003ç08ç[1-34-10,2-33-1.50,3-40-0.10]çRenato",
                                "1234567890abcd",
                                "",
                                null);

    //when
    List<Cliente> clientes = DadosClientesHelper.obter(dados);

    //then
    assertEquals(2, clientes.size());

    Cliente cliente = clientes.get(0);
    assertEquals(Long.valueOf(2345675433444345L), cliente.getCnpj());
    assertEquals("Eduardo Pereira", cliente.getNome());
    assertEquals("Rural", cliente.getAreaTrabalho());

    cliente = clientes.get(1);
    assertEquals(Long.valueOf(2345675434544345L), cliente.getCnpj());
    assertEquals("Jose da Silva", cliente.getNome());
    assertEquals("Rural", cliente.getAreaTrabalho());
  }

  @Test
  public void deveObterDadosSemClientes() {
    //given
    List<String> dados = asList("008ç2345675434544345çJose da SilvaçRural",
                                "001ç3245678865434çRenatoç40000.99",
                                "008ç2345675433444345çEduardo PereiraçRural",
                                "003ç08ç[1-34-10,2-33-1.50,3-40-0.10]çRenato",
                                "1234567890abcd",
                                "",
                                null);

    //when
    List<Cliente> clientes = DadosClientesHelper.obter(dados);

    //then
    assertTrue(clientes.isEmpty());
  }

}
