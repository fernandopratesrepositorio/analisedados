package br.com.cwi.analisedados.application.utils.dadosvendas;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import br.com.cwi.analisedados.domain.model.dadosvenda.DadosVenda;
import br.com.cwi.analisedados.domain.model.dadosvenda.Item;

public class DadosVendasHelperTest {

  @Test
  public void deveValidarDadosQuandoNull() {
    //given
    List<String> dados = null;

    //when
    List<DadosVenda> vendas = DadosVendasHelper.obter(dados);

    //then
    assertTrue(vendas.isEmpty());
  }

  @Test
  public void deveValidarDadosQuandoEmpty() {
    //given
    List<String> dados = new ArrayList<>();

    //when
    List<DadosVenda> vendas = DadosVendasHelper.obter(dados);

    //then
    assertTrue(vendas.isEmpty());
  }

  @Test
  public void deveObterDadosVendas() {
    //given
    List<String> dados = asList("002ç2345675434544345çJose da SilvaçRural",
                                "003ç10ç[1-10-100,2-30-2.50,3-40-3.10]çDiego",
                                "002ç2345675433444345çEduardo PereiraçRural",
                                "003ç08ç[1-34-10,2-33-1.50,3-40-0.10]çRenato",
                                "003ç99ç[asdhkf123]çJoao",
                                "1234567890abcd",
                                "",
                                null);

    //when
    List<DadosVenda> vendas = DadosVendasHelper.obter(dados);

    //then
    assertEquals(3, vendas.size());

    DadosVenda dadoVenda = vendas.get(0);
    assertEquals(Long.valueOf(8L), dadoVenda.getId());
    assertEquals("Renato", dadoVenda.getNomeVendedor());
    assertEquals(Double.valueOf(393.5), dadoVenda.getValorTotal());

    List<Item> itens = dadoVenda.getItens();
    assertEquals(3, itens.size());

    Item item = itens.get(0);
    assertEquals(Long.valueOf(1L), item.getId());
    assertEquals(Long.valueOf(34L), item.getQuantidade());
    assertEquals(Double.valueOf(10), item.getValorUnitario());
    assertEquals(Double.valueOf(340), item.getValorTotal());

    item = itens.get(1);
    assertEquals(Long.valueOf(2L), item.getId());
    assertEquals(Long.valueOf(33L), item.getQuantidade());
    assertEquals(Double.valueOf(1.50), item.getValorUnitario());
    assertEquals(Double.valueOf(49.5), item.getValorTotal());

    item = itens.get(2);
    assertEquals(Long.valueOf(3L), item.getId());
    assertEquals(Long.valueOf(40L), item.getQuantidade());
    assertEquals(Double.valueOf(0.10), item.getValorUnitario());
    assertEquals(Double.valueOf(4), item.getValorTotal());

    dadoVenda = vendas.get(1);
    assertEquals(Long.valueOf(10L), dadoVenda.getId());
    assertEquals("Diego", dadoVenda.getNomeVendedor());
    assertEquals(Double.valueOf(1199), dadoVenda.getValorTotal());

    itens = dadoVenda.getItens();
    assertEquals(3, itens.size());

    item = itens.get(0);
    assertEquals(Long.valueOf(1L), item.getId());
    assertEquals(Long.valueOf(10L), item.getQuantidade());
    assertEquals(Double.valueOf(100), item.getValorUnitario());
    assertEquals(Double.valueOf(1000), item.getValorTotal());

    item = itens.get(1);
    assertEquals(Long.valueOf(2L), item.getId());
    assertEquals(Long.valueOf(30L), item.getQuantidade());
    assertEquals(Double.valueOf(2.50), item.getValorUnitario());
    assertEquals(Double.valueOf(75), item.getValorTotal());

    item = itens.get(2);
    assertEquals(Long.valueOf(3L), item.getId());
    assertEquals(Long.valueOf(40L), item.getQuantidade());
    assertEquals(Double.valueOf(3.10), item.getValorUnitario());
    assertEquals(Double.valueOf(124), item.getValorTotal());

    dadoVenda = vendas.get(2);
    assertEquals(Long.valueOf(99L), dadoVenda.getId());
    assertEquals("Joao", dadoVenda.getNomeVendedor());
    assertTrue(dadoVenda.getItens().isEmpty());
  }

  @Test
  public void deveObterDadosSemClientes() {
    //given
    List<String> dados = asList("008ç2345675434544345çJose da SilvaçRural",
                                "009ç10ç[1-10-100,2-30-2.50,3-40-3.10]çDiego",
                                "008ç2345675433444345çEduardo PereiraçRural",
                                "009ç08ç[1-34-10,2-33-1.50,3-40-0.10]çRenato",
                                "1234567890abcd",
                                "",
                                null);

    //when
    List<DadosVenda> vendas = DadosVendasHelper.obter(dados);

    //then
    assertTrue(vendas.isEmpty());
  }

}
