package br.com.cwi.analisedados.application.utils.dadosvendedores;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import br.com.cwi.analisedados.domain.model.vendedor.Vendedor;

public class DadosVendedoresHelperTest {

  @Test
  public void deveValidarDadosQuandoNull() {
    //given
    List<String> dados = null;

    //when
    List<Vendedor> vendedores = DadosVendedoresHelper.obter(dados);

    //then
    assertTrue(vendedores.isEmpty());
  }

  @Test
  public void deveValidarDadosQuandoEmpty() {
    //given
    List<String> dados = new ArrayList<>();

    //when
    List<Vendedor> vendedores = DadosVendedoresHelper.obter(dados);

    //then
    assertTrue(vendedores.isEmpty());
  }

  @Test
  public void deveObterDadosVendedores() {
    //given
    List<String> dados = asList("001ç1234567891234çDiegoç50000",
                                "001ç3245678865434çRenatoç40000.99",
                                "002ç2345675433444345çEduardo PereiraçRural",
                                "003ç08ç[1-34-10,2-33-1.50,3-40-0.10]çRenato",
                                "1234567890abcd",
                                "",
                                null);

    //when
    List<Vendedor> vendedores = DadosVendedoresHelper.obter(dados);

    //then
    assertEquals(2, vendedores.size());

    Vendedor vendedor = vendedores.get(0);
    assertEquals(Long.valueOf(1234567891234L), vendedor.getCpf());
    assertEquals("Diego", vendedor.getNome());
    assertEquals(Double.valueOf(50000), vendedor.getSalario());

    vendedor = vendedores.get(1);
    assertEquals(Long.valueOf(3245678865434L), vendedor.getCpf());
    assertEquals("Renato", vendedor.getNome());
    assertEquals(Double.valueOf(40000.99), vendedor.getSalario());
  }

  @Test
  public void deveObterDadosSemVendedores() {
    //given
    List<String> dados = asList("009ç1234567891234çDiegoç50000",
                                "009ç3245678865434çRenatoç40000.99",
                                "002ç2345675433444345çEduardo PereiraçRural",
                                "003ç08ç[1-34-10,2-33-1.50,3-40-0.10]çRenato",
                                "1234567890abcd",
                                "",
                                null);

    //when
    List<Vendedor> vendedores = DadosVendedoresHelper.obter(dados);

    //then
    assertTrue(vendedores.isEmpty());
  }

}
