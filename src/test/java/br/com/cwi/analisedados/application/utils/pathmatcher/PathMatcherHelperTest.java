package br.com.cwi.analisedados.application.utils.pathmatcher;

import static br.com.cwi.analisedados.application.utils.Constantes.EXTENSAO_DAT;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.nio.file.Path;

import org.junit.Test;

public class PathMatcherHelperTest {

  @Test
  public void deveValidarArquivoComExtensaoDat() {
    //given
    Path arquivo = new File("teste.dat").toPath();

    //when
    boolean valido = PathMatcherHelper.validar(EXTENSAO_DAT, arquivo);

    //then
    assertTrue(valido);
  }

  @Test
  public void deveValidarArquivoComExtensaoDiferenteDeDat() {
    //given
    Path arquivo = new File("teste.dats").toPath();

    //when
    boolean valido = PathMatcherHelper.validar(EXTENSAO_DAT, arquivo);

    //then
    assertFalse(valido);
  }

  @Test
  public void deveValidarArquivoComExtensaoNull() {
    //given
    Path arquivo = new File("teste.dat").toPath();

    //when
    boolean valido = PathMatcherHelper.validar(null, arquivo);

    //then
    assertFalse(valido);
  }

  @Test
  public void deveValidarArquivoComExtensaoEmpty() {
    //given
    Path arquivo = new File("teste.dat").toPath();

    //when
    boolean valido = PathMatcherHelper.validar("", arquivo);

    //then
    assertFalse(valido);
  }

  @Test
  public void deveValidarArquivoNull() {
    //given
    Path arquivo = null;

    //when
    boolean valido = PathMatcherHelper.validar(EXTENSAO_DAT, arquivo);

    //then
    assertFalse(valido);
  }

  @Test
  public void deveValidarArquivoNullComExtensaoNull() {
    //given/when
    boolean valido = PathMatcherHelper.validar(null, null);

    //then
    assertFalse(valido);
  }

}
